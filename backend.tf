terraform {
  backend "gcs" {
    bucket = "gke-tf-demo-277513-tfstate"
    #credentials = file("~/.ssh/gke-tf-demo-277513-e39a8f6bf07b.json")
    credentials = "./creds/serviceaccount.json"
  }
}