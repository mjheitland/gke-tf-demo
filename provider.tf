provider "google" {
  #credentials = file("~/.ssh/gke-tf-demo-277513-e39a8f6bf07b.json")
  credentials = file("./creds/serviceaccount.json")
  project     = "gke-tf-demo-277513"
  region      = "europe-west3"
}