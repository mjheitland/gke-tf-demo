# https://www.terraform.io/docs/providers/google/r/container_cluster.html
resource "google_container_cluster" "gke-cluster" {
  name               = "my-first-gke-cluster"
  network            = "default"
  location           = "europe-west3-a"
  initial_node_count = 3
}